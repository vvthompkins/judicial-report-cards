from requests_html import HTML
import pandas as pd
from constants import FIELDS_WITH_XPATHS
from pathlib import Path

""" Iterate through all sub-directories in html_output, convert the html pages found in them to dataframes, and collect
    all those dataframes into a single CSV
 """

# TODO: Make this function able to receive a specific directory or list/range thereof


def merge_html():

    all_dirs = Path('html_output').iterdir()
    all_dfs = []

    for subdir in all_dirs:
        subdir_path = Path(subdir)
        all_html_files = subdir_path.glob('*.html')

        for file in all_html_files:
            print(file.name)
            defendant_df = pd.DataFrame(columns=FIELDS_WITH_XPATHS.keys())

            with open(file) as infile:

                source_code = infile.read()
                case_html = HTML(html=source_code)
                case_sections = case_html.find(".crim-history-row")

                for i in range(len(case_sections)):
                    row = []
                    for xpath in FIELDS_WITH_XPATHS.values():
                        element = case_sections[i].xpath(xpath, first=True)
                        value = ''
                        if element:
                            value = element.text
                        row.append(value)
                    defendant_df.loc[i] = row

            all_dfs.append(defendant_df)

    merged = pd.concat(all_dfs).drop_duplicates()

    with open('csv_output/all_results.csv', 'w+') as outfile:
        merged.to_csv(outfile, index=False)


if __name__ == "__main__":
    merge_html()

